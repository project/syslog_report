/**
 * @file 
 * syslog accordion js
 */
(function ($) {
    $(".syslog_report").accordion({
        header: "tr.log_row",
        icons: false
    });
})(jQuery);