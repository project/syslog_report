-- SUMMARY --

The Syslog report module display logs of syslog as an Administrator.
Administrator can filter the syslog report by search box. Search box is case-sensitive

-- REQUIREMENTS --
Enable syslog core module

-- INSTALLATION --

Install as usual, see https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for further information.

-- CONFIGURATION --
Administrator can give the permission "view syslog report" to view syslog report
Syslog will keep all system logs. Modify syslog identity on http://example.com/admin/config/development/logging

Enter syslog file full path, visit the project page
 http://example.com/admin/config/syslog-report
 
-- Output --
To view syslog report, visit the project page
 http://example.com/admin/reports/syslog-report

